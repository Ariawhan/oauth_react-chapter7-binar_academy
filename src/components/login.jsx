import React, { Component, useEffect } from "react";
import { GoogleLogin, GoogleLogout } from "react-google-login";
import { gapi } from "gapi-script";

class Login extends Component {
  responseGoogle = (response) => {
    console.log(response.profileObj.email);
    console.log(response);
    //Disipen ke localStorage/di jassin session
    //Di konsumsi backend
  };

  logout = () => {
    console.log("Suksess logout");
    //Clear LocalStorage (dev)
    //Clear Session (dev)
  };

  useEffect = () => {
    function start() {
      gapi.client.init({
        clientId:
          "765560781813-6thv8i87m61oul5onp03q18l6bbteelo.apps.googleusercontent.com",
        scope: "",
      });
    }
    gapi.load("client:auth2", start);
  };

  render() {
    return (
      <div id="Login">
        <GoogleLogin
          clientId="765560781813-6thv8i87m61oul5onp03q18l6bbteelo.apps.googleusercontent.com"
          buttonText="Login Googlenya Gusde"
          onSuccess={this.responseGoogle}
          onFailure={this.responseGoogle}
          cookiePolicy={"single_host_origin"}
        />
        <GoogleLogout
          clientId="765560781813-6thv8i87m61oul5onp03q18l6bbteelo.apps.googleusercontent.com"
          buttonText="Logout"
          onLogoutSuccess={this.logout}
        ></GoogleLogout>
      </div>
    );
  }
}
export default Login;
